# Install
To install or update simply run the following command.
Note that this command will overwrite your dotfiles so take care.

```bash -c "$(curl -fsSL https://gitlab.com/jhring/dotfiles/raw/master/bin/install_dotfiles)"```
